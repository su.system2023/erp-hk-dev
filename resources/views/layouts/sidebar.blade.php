<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand" href="{{asset('template')}}/pages/dashboards/dashboard.html">
          <img src="{{asset('template')}}/assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                <i class="ni ni-badge text-orange"></i>
                <span class="nav-link-text">Human Resources</span>
              </a>
              <div class="collapse" id="navbar-examples">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="{{url('data-karyawan')}}" class="nav-link">Data Karyawan</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{asset('template')}}/pages/examples/pricing.html" class="nav-link">Absensi</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{asset('template')}}/pages/examples/pricing.html" class="nav-link">Payroll</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{asset('template')}}/pages/examples/pricing.html" class="nav-link">Recruitment</a>
                  </li>
                </ul>
              </div>
            </li>
            
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">Setting</h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            
            <li class="nav-item">
              <a class="nav-link" href="{{url('user-setting')}}">
                <i class="ni ni-single-02"></i>
                <span class="nav-link-text">User</span>
              </a>
            </li>
            
          </ul>
        </div>
      </div>
    </div>
  </nav>