<!DOCTYPE html>
 <html>

 <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Pembukuan Online.">
  <title>Pembukuan Online</title>
  <!-- Favicon -->
  <link rel="icon" href="{{asset('template')}}/assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="{{asset('template')}}/assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="{{asset('template')}}/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->

  <link rel="stylesheet" href="{{asset('template')}}/assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('template')}}/assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('template')}}/assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">

  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{asset('template')}}/assets/css/argon.css?v=1.1.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.sidebar')
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('layouts.topbar')


    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-10">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Default</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Default</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
          <!-- Card stats -->
          @yield('header-content')
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--10">
      @yield('body-content')
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{asset('template')}}/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="{{asset('template')}}/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="{{asset('template')}}/assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <!-- Optional JS -->
  <script src="{{asset('template')}}/assets/vendor/select2/dist/js/select2.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/nouislider/distribute/nouislider.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/quill/dist/quill.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/dropzone/dist/min/dropzone.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/onscreen/dist/on-screen.umd.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/moment/min/moment.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="{{asset('template')}}/assets/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/datetime/1.0.3/js/dataTables.dateTime.min.js"></script>

  <!-- Argon JS -->
  <script src="{{asset('template')}}/assets/js/argon.js?v=1.1.0"></script>
  <!-- Demo JS - remove this in your project -->
  <script src="{{asset('template')}}/assets/js/demo.min.js"></script>

</body>

</html>