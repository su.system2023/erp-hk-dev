<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataKaryawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_karyawans', function (Blueprint $table) {
            $table->id();
            $table->string('nik')->nullable();
            $table->String('nama_karyawan')->nullable();
            $table->string('no_ktp')->nullable();
            $table->string('tanggal_join')->nullable();
            $table->string('tanggal_lahir')->nullable();
            $table->string('umur')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('lokasi_kerja')->nullable();
            $table->string('department')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->string('tanggal_berhenti_kerja')->nullable();
            $table->string('status')->nullable();
            $table->string('project')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_karyawans');
    }
}
