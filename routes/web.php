<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //Modul Setting

    //User
    Route::get('/user-setting',[App\Http\Controllers\UserSettingController::class,'index'])->name('user-setting');

    //Modul Human Resources

    //Data Karyawan
    Route::get('/data-karyawan',[App\Http\Controllers\DataKaryawanController::class,'index'])->name('data-karyawan');

});