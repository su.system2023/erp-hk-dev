<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserSettingController extends Controller
{
    public function index(){
        $users = User::all();
        return view('settings.user-setting.index')->with(compact('users'));
    }
}
