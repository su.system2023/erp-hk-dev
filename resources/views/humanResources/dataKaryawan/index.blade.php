@php
    $now = Carbon\Carbon::today('Asia/Makassar')->format('d M Y');
@endphp
@extends('layouts.layout')

@section('header-content')

                @if(session()->has('success'))
		            <div class="col-12 mb-4">
		              <div class="alert alert-success alert-dismissible fade show" role="alert">
		                <span class="alert-inner--icon"><i class="ni ni-notification-70"></i></span>
		                <strong> {{ session('success') }} </strong>
		                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		            </div>
		            @endif

		            @if(session()->has('danger'))
		            <div class="col-12 mb-4">
		              <div class="alert alert-danger alert-dismissible fade show" role="alert">
		                <span class="alert-inner--icon"><i class="ni ni-notification-70"></i></span>
		                <strong> {{ session('danger') }} </strong>
		                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		            </div>
		            @endif


<div class="row">
  <div class="col-xl-3 col-md-6">
    <div class="card card-stats">
      <!-- Card body -->
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-muted mb-0">Total traffic</h5>
            <span class="h2 font-weight-bold mb-0">350,897</span>
          </div>
          <div class="col-auto">
            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
              <i class="ni ni-active-40"></i>
            </div>
          </div>
        </div>
        <p class="mt-3 mb-0 text-sm">
          <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
          <span class="text-nowrap">Since last month</span>
        </p>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6">
    <div class="card card-stats">
      <!-- Card body -->
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
            <span class="h2 font-weight-bold mb-0">2,356</span>
          </div>
          <div class="col-auto">
            <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
              <i class="ni ni-chart-pie-35"></i>
            </div>
          </div>
        </div>
        <p class="mt-3 mb-0 text-sm">
          <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
          <span class="text-nowrap">Since last month</span>
        </p>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6">
    <div class="card card-stats">
      <!-- Card body -->
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
            <span class="h2 font-weight-bold mb-0">924</span>
          </div>
          <div class="col-auto">
            <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
              <i class="ni ni-money-coins"></i>
            </div>
          </div>
        </div>
        <p class="mt-3 mb-0 text-sm">
          <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
          <span class="text-nowrap">Since last month</span>
        </p>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6">
    <div class="card card-stats">
      <!-- Card body -->
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
            <span class="h2 font-weight-bold mb-0">49,65%</span>
          </div>
          <div class="col-auto">
            <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
              <i class="ni ni-chart-bar-32"></i>
            </div>
          </div>
        </div>
        <p class="mt-3 mb-0 text-sm">
          <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
          <span class="text-nowrap">Since last month</span>
        </p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('body-content')

<div class="row justify-content-center">

    <div class="col-lg-11 col-12 mt-4">
      {{-- Jadwal Reservasi --}}
      <div class="row mb-3 mt-2 align-items-center justify-content-center">
        <div class="col-12">
          <h3 class="mb-0 font-weight-bolder">Data Karyawan <sup class="badge badge-sm badge-danger">New</sup></h3>
        </div>
      </div>
      <div class="row mb-4">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header bg-gradient-info border-0 py-4">
              <div class="row">
                <div class="col-lg-6 col-6 theader">
                  <span class="h5 text-white"><i class="far fa-calendar-alt mr-2 text-white"></i>Hari Ini : </span>
                  <span class="h5 text-white">{{$now}}</span>
                </div>
              </div>
            </div>

            <div class="table-responsive border-0 py-3">
              <table class=" table table-hover table-flush datatable-dataKaryawan">
                <thead class="thead-light">
                  <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">NIK</th>
                    <th class="text-center">Nama Karyawan</th>
                    <th class="text-center">No.KTP</th>
                    <th class="text-center">Tanggal Join</th>
                    <th class="text-center">Tanggal Lahir</th>
                    <th class="text-center">Umur</th>
                    <th class="text-center">Jenis Kelamin</th>
                    <th class="text-center">Lokasi Kerja</th>
                    <th class="text-center">Department</th>
                    <th class="text-center">Jabatan</th>
                    <th class="text-center">Nama Perusahaan</th>
                    <th class="text-center">Tanggal Berhenti Kerja</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Project</th>
                    <th class="text-center"></th>
                  </tr>
                </thead>
                <tbody class="userSet">
                  @foreach ($users as $user)
                      <tr>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      {{-- End Jadwal Reservasi --}}
    </div>
  </div>


  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script type="text/javascript">

$(document).ready(function () {
      $('.datatable-dataKaryawan' , function() {
				$('.dt-buttons .btn').removeClass('btn-secondary').addClass('btn-sm btn-warning mb-3');
			}).DataTable({
				order:[[0,"desc"]],
				pageLength: 10,
				lengthMenu: [10, 25, 50],
				language: {
					paginate: {
						previous: "<i class='fas fa-angle-left'>",
						next: "<i class='fas fa-angle-right'>"
					}
				},
				dom: 'frtlip',
				buttons: [
				'copy', 'pdf', 'print'
				]
			});
    });

</script>
@endsection