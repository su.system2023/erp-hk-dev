<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class DataKaryawanController extends Controller
{
    public function index(){
        $users = User::all();
        return view('humanResources.dataKaryawan.index')->with(compact('users'));
    }
}
